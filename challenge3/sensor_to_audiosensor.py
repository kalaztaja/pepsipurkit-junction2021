import pandas as pd

# [distance sensorid audiosensorid]
def SensorToAudioSensor(sensorId):

    dat = pd.read_json('distances.json')
    dat = dat[dat['deviceid'].astype(int) == sensorId]
    if(len(dat) == 0):
        return None
    dat = dat.sort_values(by=['dist'])
    return dat.audioDevice.values[0]
