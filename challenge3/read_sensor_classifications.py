import pandas as pd
import os
import glob
import re

def ImportSensorClassifications(filenaming):
    sensorClasses = {}

    cwd = os.getcwd()
    sensorFiles = []

    for root, subdirectories, files in os.walk(cwd):
        for subdirectory in subdirectories:
            if "audio" in subdirectory:
                f = os.path.join(root, subdirectory)
                for filename in glob.glob(os.path.join(f, filenaming)):
                    if "classification" in filename:
                        sensorFiles.append(filename)

    for file in sensorFiles:
        data = pd.read_json(file)
        nbr = re.findall(r'\d+', file)[-1]
        sensorClasses[nbr] = data

    return sensorClasses