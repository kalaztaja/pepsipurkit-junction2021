from sensor_to_audiosensor import SensorToAudioSensor
import numpy as np
import re
from classifier import getFeatures, predict


def AddClassifcationData(df, sensorClassification):

    groupIds = df['groupid'].unique()

    allClasses = {}

    for groupId in groupIds:
        try:
            val = int(groupId)
        except ValueError:
            continue
        groupData = df[df.groupid == groupId]
        classes = []

        for index, row in groupData.iterrows():
            timestamp = float(row['timestamp'])
            sensorId = int(row['deviceid'])
            audio_sensor = SensorToAudioSensor(sensorId)
            if audio_sensor is not None:
                audio_id = re.findall(r'\d+', audio_sensor)[-1]
                audioClasses = sensorClassification[audio_id]
                timestampClass = audioClasses[(audioClasses.timeStart <= timestamp) & (
                    audioClasses.timeEnd > timestamp)]
                if not timestampClass.empty:
                    foundClass = timestampClass.sound.values[0]
                    classes.append(foundClass)
        if len(classes) > 0:
            unique, pos = np.unique(classes, return_inverse=True)
            counts = np.bincount(pos)
            maxpos = counts.argmax()
            allClasses[groupId] = unique[maxpos]

    for key in allClasses:
        df.loc[df['groupid'] == key,
               'objecttype'] = allClasses[key]
    df['objecttype'] = df.apply(
        lambda x: str(x['objecttype']).encode(errors='replace'), axis=1)

    return df


def AddPythonClassifcationData(df):

    groupIds = df['groupid'].unique()

    allClasses = {}

    for groupId in groupIds:
        try:
            val = int(groupId)
        except ValueError:
            continue
        groupData = df[df.groupid == groupId]
        classes = []

        for index, row in groupData.iterrows():
            timestamp = float(row['timestamp'])
            sensorId = int(row['deviceid'])
            audio_sensor = SensorToAudioSensor(sensorId)
            if audio_sensor is not None:
                audio_id = re.findall(r'\d+', audio_sensor)[-1]
                x = getFeatures(audio_id, 1, timestamp)
                res = predict(x)
                if len(res) > 0:
                    classes.append(res)
        if len(classes) > 0:
            unique, pos = np.unique(classes, return_inverse=True)
            counts = np.bincount(pos)
            maxpos = counts.argmax()
            allClasses[groupId] = unique[maxpos]
    for key in allClasses:
        df.loc[df['groupid'] == key, 'objecttype'] = allClasses[key]

    return df
