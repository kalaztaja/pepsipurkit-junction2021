import pandas as pd

# Writing df to json. Takes care of rows which have empty ObjectID and also converts deviceID to coordinates 
def AddSensorLocation(df_data):

    df_device_data=pd.read_json(f'./audio_data/site_6.json')

    #Drop rows if ObjectID = 0 or null
    df_data = df_data.reset_index(drop=True)
    for index, row in df_data.iterrows():
        if not row['groupid']:
            df_data.drop(index, inplace=True)
        else:
            df_data.at[index,"x"] = df_device_data.loc[row["deviceid"],"x"]
            df_data.at[index,"y"] = df_device_data.loc[row["deviceid"],"y"]
    return df_data
