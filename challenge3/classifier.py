from matplotlib import pyplot as plt
import librosa
import librosa.display
import numpy as np
from moment_splitter import moment_splitter
from extract_features import extract_features
from pathlib import Path
from joblib import dump, load
import pickle
from sklearn import preprocessing


def getFeatures(sensorid=1, duration=1, offset=0):
    try:
        aud, SR, filename = moment_splitter(sensor=sensorid, duration=duration,
                                            offset=offset, highpass=8000, lowpass=1, bandstop1=[0, 0], bandstop2=[0, 0])
        feat = np.array(extract_features(file=filename, aud=aud))
        feat = feat.reshape(1, -1)
        return feat
    except:
        return None


def predict(x):
    try:
        clf = load('clf.joblib')
        le = load('le.joblib')
        pred = clf.predict(x)
        return str(le.inverse_transform(pred)[0])
    except:
        return u"Unknown"
