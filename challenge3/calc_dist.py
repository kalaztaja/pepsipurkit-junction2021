import numpy as np

def calc_dist(x1,y1,x2,y2):
    return np.sqrt(np.square(x1-x2)+np.square(y1-y2))