from matplotlib import pyplot as plt
import librosa
import librosa.display
import numpy as np
import scipy as sp
from scipy.fft import fft
from scipy.io.wavfile import read
from scipy.io.wavfile import write
from scipy import signal
from pathlib import Path


def extract_features(file='./audio_data/audio_sensor_1_filtered.wav', aud=None):
    if(aud is not None):
        signals = aud
    else:
        signals = [librosa.load(p)[0] for p in Path().glob(file)]
    features = np.array([_extract_features(x) for x in signals])

    return [features.min(), features.max(), features.mean()]


def _extract_features(signal):
    return librosa.feature.rms(signal)
