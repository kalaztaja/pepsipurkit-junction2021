import anime from 'animejs';
import { useRef } from 'react';

function AnimationLayer() {

  const animationRef = useRef(null);
  var animeTimeline = useRef();


  animeTimeline.current = anime.timeline({
    targets: animationRef.current,
    easing: 'easeInOutSine',
    duration: 1000,
    loop: true,
    direction: 'alternate',

    autoplay: false,
  });
  animeTimeline.current.add({
    targets: animationRef.current,
    translateX: 250,
  })
  return (
    <div class="flex">



      <div className="el" ref={animationRef} />
      <div className="el" ref={animeTimeline} />
      <div>
        <button onClick={() => animeTimeline.current.restart()}>Restart</button>

        <button onClick={() => animeTimeline.current.play()}>Play</button>
        <button onClick={() => animeTimeline.current.pause()}>Pause</button>
      </div>
    </div>
  )
}

export default AnimationLayer;